GO ?= $(shell command -v go 2> /dev/null)
NPM ?= $(shell command -v npm 2> /dev/null)
MANIFEST_FILE ?= plugin.json

# PLUGIN_ID and PLUGIN_VERSION must match their respective values in plugin.json file
PLUGIN_ID ?= com.collabora.profile-plus
PLUGIN_VERSION ?= 0.1.1
BUNDLE_NAME ?= $(PLUGIN_ID)-$(PLUGIN_VERSION).tar.gz

# Builds the server for all supported architectures.
.PHONY: server
server:
	mkdir -p dist;
	cd server && env GOOS=linux GOARCH=amd64 $(GO) build -o dist/plugin-linux-amd64;
	cd server && env GOOS=darwin GOARCH=amd64 $(GO) build -o dist/plugin-darwin-amd64;
	cd server && env GOOS=windows GOARCH=amd64 $(GO) build -o dist/plugin-windows-amd64.exe;

# Ensures NPM dependencies are installed without having to run this all the time.
webapp/node_modules: $(wildcard webapp/package.json)
	cd webapp && $(NPM) ci
	touch $@

# Builds the webapp.
.PHONY: webapp
webapp: webapp/node_modules
	cd webapp && $(NPM) run build;

# Generates a tar bundle of the plugin for install.
.PHONY: bundle
bundle:	server webapp
	rm -rf dist/
	mkdir -p dist/$(PLUGIN_ID)
	cp $(MANIFEST_FILE) dist/$(PLUGIN_ID)/
	mkdir -p dist/$(PLUGIN_ID)/server
	cp -r server/dist dist/$(PLUGIN_ID)/server/
	mkdir -p dist/$(PLUGIN_ID)/webapp
	cp -r webapp/dist dist/$(PLUGIN_ID)/webapp/
	cd dist && tar -cvzf $(BUNDLE_NAME) $(PLUGIN_ID)

	@echo plugin built at: dist/$(BUNDLE_NAME)

# Clean removes all build artifacts.
.PHONY: clean
clean:
	rm -fr dist/
	rm -fr server/dist
	rm -fr webapp/dist
