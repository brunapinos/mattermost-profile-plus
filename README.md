# mattermost-profile-plus

A plugin that allows Mattermost to display extra details in the user profile.


## Installation

To download and build the plugin run:

```
git clone https://gitlab.collabora.com/tools/mattermost-profile-plus
make bundle
```

The commands above will generate a `plugin.tar.gz` file in the `dist/` folder
that can be uploaded to the Mattermost server in the admin plugin management page.

## License

**mattermost-profile-plus** is distributed under the [LGPL-3.0 license](LICENSE).

## Configuration

This plugin provides no backend for storing and managing the user statuses and
badges, it aims instead to make it easy to consume external APIs for that purpose.
This way you can for example hook up this plugin with your organization internal
system.

Before starting using this plugin you need to configure it first in the
Mattermost system console, under the `Plugins > Profile Plus` menu. There you must
tick the `Enable Plugin` checkbox and fill the following settings:

- `API URL Template` - the URL scheme for the API endpoint for retrieving the
  user profile. It must map to a URL that somehow contains the Mattermost username
  for the user we want to fetch the badges to. Therefore it must include
  the string `{username}` to be interpolated against, e.g
  `https://api.myservice.com/v2/users/{username}/`
- `API HTTP Headers` - if your API requires any specific headers to be consumed
  you can use this field for setting up the key-value pairs in JSON format, like
  so:

```json
{
    "Accept": "application/json",
    "Authorization": "Bearer 1234567890
}
```

The API pointed out by the `API URL Template` must return a JSON object
containing the user status and a list of all of the user badges in the following
format, where `title` is the content of the text that will appear inside the
badge and `color` is the background color:

```json
GET "https://api.myservice.com/v2/users/johndoe"

{
  "status": "Feeling good today",
  "badges": [
    {
        "title": "My first badge",
        "color": "#ffffff"
    },
    {
        "title": "My second badge",
        "color": "#000000"
    }
  ]
}
```