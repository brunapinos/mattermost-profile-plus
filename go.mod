module gitlab.collabora.com/tools/mattermost-profile-plus

go 1.14

require github.com/mattermost/mattermost-server/v5 v5.32.1
