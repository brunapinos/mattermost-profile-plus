package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/mattermost/mattermost-server/v5/plugin"
)

// ProfilePlusPlugin implements the interface expected by the Mattermost server
// to communicate between the server and plugin processes.
type ProfilePlusPlugin struct {
	plugin.MattermostPlugin
}

// Badge contains both the title and the color that represents a user badge
type Badge struct {
	Title string `json:"title"`
	Color string `json:"color"`
}

// Profile contains a set of attributes that describe the user
type Profile struct {
	Status string  `json:"status"`
	Badges []Badge `json:"badges"`
}

func (p *ProfilePlusPlugin) ServeHTTP(c *plugin.Context, w http.ResponseWriter, r *http.Request) {
	username := strings.TrimPrefix(r.URL.Path, "/api/v1/users/")
	if username == "" {
		http.NotFound(w, r)
		return
	}
	if r.Header.Get("Mattermost-User-Id") == "" {
		http.Error(w, "You are not authorized to access this page", http.StatusUnauthorized)
		return
	}

	// Load plugin configuration (API URL template and headers)
	plugin := p.API.GetConfig().PluginSettings.Plugins["com.collabora.profile-plus"]
	apiURLTemplate := fmt.Sprintf("%v", plugin["api_url_template"])
	apiHTTPHeaders := fmt.Sprintf("%v", plugin["api_http_headers"])

	url := strings.Replace(apiURLTemplate, "{username}", username, 1)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		http.Error(w, "Error creating request to external API", http.StatusInternalServerError)
		return
	}

	// Add the headers to the request
	headers := make(map[string]string)
	json.Unmarshal([]byte(apiHTTPHeaders), &headers)
	for k, v := range headers {
		req.Header.Add(k, v)
	}

	// Fetch the external API
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, "Error making a request to external API", http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Handle external API HTTP errors
	if resp.StatusCode != 200 {
		body, _ := ioutil.ReadAll(resp.Body)
		http.Error(w, string(body), resp.StatusCode)
		return
	}

	// Parse the external API response
	profile := new(Profile)
	json.NewDecoder(resp.Body).Decode(profile)

	// Prepare the plugin response
	if content, err := json.Marshal(profile); err != nil {
		http.Error(w, "Error encoding the profile to JSON", http.StatusInternalServerError)
	} else {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprint(w, string(content))
	}

}

func main() {
	plugin.ClientMain(&ProfilePlusPlugin{})
}
