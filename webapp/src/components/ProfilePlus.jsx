// Copyright 2021, Collabora Ltd.
// SPDX-License-Identifier: LGPL-3.0-or-later

import { useState, useEffect } from "react";
import { Client4 } from "mattermost-redux/client";

import Badge from "./Badge";

function ProfilePlus(props) {
  const empty = { status: "", badges: [] };
  const [user, setUser] = useState(empty);

  useEffect(() => {
    const pluginId = "com.collabora.profile-plus";
    const url = `/plugins/${pluginId}/api/v1/users/${props.user.username}`;

    fetch(url, Client4.getOptions({ method: "GET" }))
      .then((res) => (res.ok ? res.json() : empty))
      .then((user) => setUser(user))
      .catch((error) => setUser(empty));
  }, []);

  const badgesListStyle = {
    display: "flex",
    flexFlow: "row wrap",
    padding: "0",
    margin: "0",
    listStyle: "none",
  };
  const badgesItemStyle = {
    margin: "0 2px 0 0",
  };

  return (
    <div>
      {user.status ? (
        <div>
          <hr class="divider divider--expanded" />
          <div class="overflow--ellipsis text-nowrap">
            <strong>{user.status}</strong>
          </div>
          {/* Only render the bottom bar if there are no badges */}
          {user.badges.length > 0 ? null : <hr class="divider divider--expanded" />}
        </div>
      ) : null}

      {user.badges.length > 0 ? (
        <div>
          <hr class="divider divider--expanded" />
          <ul style={badgesListStyle}>
            {user.badges.map((badge) => (
              <li style={badgesItemStyle}>
                <Badge color={badge.color} title={badge.title} />
              </li>
            ))}
          </ul>
          <hr class="divider divider--expanded" />
        </div>
      ) : null}
    </div>
  );
}

export default ProfilePlus;
